build-depends:
- components/bison.bst
- components/flex.bst
- components/llvm.bst
- components/python3.bst
- components/python3-mako.bst
- components/rust.bst
- components/rust-bindgen.bst
- components/spirv-llvm-translator.bst
- components/spirv-tools.bst
- components/vulkan-headers.bst
- components/wayland-protocols.bst
- components/glslang.bst
- public-stacks/buildsystem-meson.bst

depends:
- bootstrap-import.bst
- components/libdrm.bst
- components/libva.bst
- components/opencl.bst
- components/xorg-lib-xfixes.bst
- components/xorg-lib-xrandr.bst
- components/xorg-lib-xshmfence.bst
- components/xorg-lib-xxf86vm.bst
- components/vulkan-icd-loader.bst
- components/wayland.bst
- components/libglvnd.bst
- components/libvdpau.bst
- components/libunwind.bst
- components/zstd.bst
- extensions/mesa/libclc.bst
- extensions/mesa/libdrm.bst
- extensions/mesa/llvm.bst

(@):
- elements/extensions/mesa/config.yml
- elements/extensions/mesa/mesa-sources.yml

environment:
  PKG_CONFIG_PATH: "%{libdir}/pkgconfig:%{datadir}/pkgconfig"
  CXXFLAGS: "%{target_flags} -std=gnu++17"

variables:
  (?):
  - target_arch == "i686" or target_arch == "x86_64":
      gallium_drivers: iris,crocus,nouveau,r300,r600,radeonsi,svga,swrast,virgl,zink,i915
      vulkan_drivers: amd,intel,intel_hasvk,swrast,virtio-experimental
      libunwind: enabled
  - target_arch == "arm" or target_arch == "aarch64":
      gallium_drivers: asahi,etnaviv,freedreno,kmsro,lima,nouveau,panfrost,swrast,tegra,virgl,v3d,vc4,zink,r600,r300,radeonsi
      vulkan_drivers: freedreno,broadcom,panfrost,swrast,amd
      libunwind: disabled
  - target_arch in ("ppc64le", "ppc64", "riscv64"):
      gallium_drivers: nouveau,r600,r300,radeonsi,swrast,virgl
      vulkan_drivers: amd
      libunwind: disabled

  optimize-debug: "false"

  meson-lto-flags: ''
  meson-local: >-
    -Db_ndebug=true
    -Ddri3=enabled
    -Degl=enabled
    -Dgallium-drivers=%{gallium_drivers}
    -Dgallium-nine=true
    -Dgallium-omx=disabled
    -Dgallium-opencl=icd
    -Dgallium-rusticl=true
    -Dgallium-va=enabled
    -Dgallium-vdpau=enabled
    -Dgallium-xa=disabled
    -Dgbm=enabled
    -Dgles1=disabled
    -Dgles2=enabled
    -Dglvnd=true
    -Dglx=auto
    -Dlibunwind=%{libunwind}
    -Dllvm=enabled
    -Dlmsensors=disabled
    -Dmicrosoft-clc=disabled
    -Dandroid-libbacktrace=disabled
    -Dosmesa=false
    -Dplatforms=x11,wayland
    -Dselinux=false
    -Dshared-glapi=enabled
    -Dvalgrind=disabled
    -Dopencl-external-clang-headers=disabled
    -Dvulkan-layers=device-select,overlay
    -Dvulkan-drivers=%{vulkan_drivers}
    -Dvulkan-icd-dir="%{libdir}/vulkan/icd.d"
    -Dxlib-lease=enabled
    -Dzstd=enabled
    -Dvideo-codecs=%{video_codecs}
    -Drust_std=2021

config:
  install-commands:
    (>):
    - |
      mkdir -p "%{install-root}%{libdir}"
      mv "%{install-root}%{sysconfdir}/OpenCL" "%{install-root}%{libdir}/"
      ln -s libEGL_mesa.so.0 %{install-root}%{libdir}/libEGL_indirect.so.0
      ln -s libGLX_mesa.so.0 %{install-root}%{libdir}/libGLX_indirect.so.0
      rm -f "%{install-root}%{libdir}"/libGLESv2*
      rm -f "%{install-root}%{libdir}/libGLX_mesa.so"
      rm -f "%{install-root}%{libdir}/libEGL_mesa.so"
      rm -f "%{install-root}%{libdir}/libglapi.so"

    - |
      for dir in vdpau dri; do
        for file in "%{install-root}%{libdir}/${dir}/"*.so*; do
          soname="$(objdump -p "${file}" | sed "/ *SONAME */{;s///;q;};d")"
          if [ -L "${file}" ]; then
            continue
          fi
          if ! [ -f "%{install-root}%{libdir}/${dir}/${soname}" ]; then
            mv "${file}" "%{install-root}%{libdir}/${dir}/${soname}"
          else
            rm "${file}"
          fi
          ln -s "${soname}" "${file}"
        done
      done

    - |
      if [ -f "%{install-root}%{includedir}/vulkan/vulkan_intel.h" ]; then
        mkdir -p "%{install-root}%{includedir}/%{gcc_triplet}/vulkan"
        mv "%{install-root}%{includedir}/vulkan/vulkan_intel.h" "%{install-root}%{includedir}/%{gcc_triplet}/vulkan/"
      fi

    - |
      ln -sr '%{install-root}%{datadir}/glvnd' '%{install-root}%{prefix}/glvnd'
      mkdir -p '%{install-root}%{prefix}/vulkan'
      ln -sr '%{install-root}%{libdir}/vulkan/icd.d' '%{install-root}%{prefix}/vulkan/icd.d'
      ln -sr '%{install-root}%{datadir}/vulkan/explicit_layer.d' '%{install-root}%{prefix}/vulkan/explicit_layer.d'
      ln -sr '%{install-root}%{datadir}/vulkan/implicit_layer.d' '%{install-root}%{prefix}/vulkan/implicit_layer.d'
      ln -sr '%{install-root}%{libdir}/OpenCL' '%{install-root}%{prefix}/OpenCL'

public:
  bst:
    split-rules:
      devel:
        (>):
        - '%{libdir}/libgbm.so'
        - '%{libdir}/libglapi.so'
        - '%{libdir}/libwayland-egl.so'
        - '%{libdir}/libMesaOpenCL.so'
        - '%{libdir}/d3d/d3dadapter9.so'
  cpe:
    product: mesa
    vendor: mesa3d
